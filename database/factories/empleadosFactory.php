<?php

use Faker\Generator as Faker;

$factory->define(App\empleados::class, function (Faker $faker) {
    return [
        "name" => $faker->name,
        "email"=> $faker->email,
        "street"=>  $faker->streetName
    ];
});
