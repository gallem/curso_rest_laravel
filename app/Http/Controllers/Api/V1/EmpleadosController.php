<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\empleados;

class EmpleadosController extends Controller
{

    public function __construct(){
        
    }
    //
    public function index(){
        try{
            $empleados = empleados::all();
            $data = [
                "status" => 1,
                "data"=>$empleados,
                "message"=>"tus datos"
            ];
            return response()->json($data,200);
        }catch(Exception $ex){
            $data = [
                "status" => 0,
                "data"=>$ex,
                "message"=>"Algo Salio Mal"
            ];
            return response()->json($data,500);
        }
    }

    public function create(Request $request){
        try{
            $empleado = new empleados();
            $empleado->name = $request->name;
            $empleado->email = $request->email;
            $empleado->street = $request->street;
            $empleado->save();

            $data = [
                "status" => 1,
                "data"=>null,
                "message"=>"Se regitro el empleado {$empleado->id}"
            ];

            return response()->json($data,200);
        }catch(Exception $ex){
            $data = [
                "status" => 0,
                "data"=>$ex,
                "message"=>"Algo Salio Mal"
            ];
            return response()->json($data,500);
        }
    }

    public function show($id){
        try{
            $empleados = empleados::find($id);
            $data = [
                "status" => 1,
                "data"=>$empleados,
                "message"=>"tus datos"
            ];
            return response()->json($data,200);
        }catch(Exception $ex){
            $data = [
                "status" => 0,
                "data"=>$ex,
                "message"=>"Algo Salio Mal"
            ];
            return response()->json($data,500);
        }
    }
}
