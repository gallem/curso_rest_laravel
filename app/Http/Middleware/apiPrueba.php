<?php

namespace App\Http\Middleware;

use Closure;

class apiPrueba
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->Header("Authorization");
        if ($token == "marco gallegos") {
            return $next($request);
        }else{
            $data = [
                "status" => 0,
                "data"=>null,
                "message"=>"no esta autorizado"
            ];
            return response()->json($data,500);
        }
    }
}
